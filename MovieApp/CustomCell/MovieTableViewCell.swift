//
//  MovieTableViewCell.swift
//  MovieApp
//
//  Created by Artekium on 15/09/2021.
//

import UIKit

class MovieTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.numberOfLines = 0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static let identifier = "MovieTableViewCell"
    
    static func nib () -> UINib {
        return UINib(nibName: "MovieTableViewCell", bundle: nil)
    }
    
    func configureCell(with movie: Movie) {
        self.titleLabel.text = movie.title
        self.dateLabel.text = movie.date
    }
    
}

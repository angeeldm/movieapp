//
//  LoginViewController.swift
//  MovieApp
//
//  Created by Artekium on 15/09/2021.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    static func nib () -> UINib {
        return UINib(nibName: "LoginViewController", bundle: nil)
    }
    
    @IBAction func loginBtn() {
        let email:String = emailField.text!
        let password:String = passwordField.text!
        
        if !email.isEmpty && email.contains("@") && !password.isEmpty {
            let mainView = MoviesViewController(nibName: "MoviesViewController", bundle: nil)
            self.present(mainView, animated: true)
        } else {
            let alert = UIAlertController(title: "Hey!", message: "Los campos son obligatorios", preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .default, handler: { action in })
            alert.addAction(action)
            present(alert, animated: true)
        }
    }
}

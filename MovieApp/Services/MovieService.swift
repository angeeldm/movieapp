//
//  MovieService.swift
//  MovieApp
//
//  Created by Artekium on 14/09/2021.
//

import Foundation
import Alamofire

final class MoviesService {
    static let shared = MoviesService()
    
    private let baseURL = "https://api.themoviedb.org/3/"
    private let APIkey = "28758a0c415d52c0c9611632e39086f4"
    
    func getMovies(success: @escaping (_ movies: MovieResponse) -> (), failure: @escaping (_ error: Error?) -> ()){
        let url = "\(baseURL)movie/popular?api_key=\(APIkey)"
        let request = AF.request(url).validate()
        request.responseDecodable(of: MovieResponse.self){
            (response) in
            if let movies = response.value {
                success(movies)
            } else {
                failure(response.error)
            }
        }
    }
    
    func getMovieById(id: Int, success: @escaping (_ movie: Movie) -> (), failure: @escaping (_ error: Error?) -> ()){
        let url = "\(baseURL)movie/\(id)?api_key=\(APIkey)"
        AF.request(url, method: .get).responseDecodable( of: Movie.self) { response in
            if let movie = response.value {
                success(movie)
            } else {
                failure(response.error)
            }
        }
    }
}

//
//  Movie.swift
//  MovieApp
//
//  Created by Artekium on 14/09/2021.
//

import Foundation

struct MovieResponse: Decodable {
    let page: Int?
    let results: [Movie]?
}

struct Movie: Decodable {
    let id: Int?
    let title: String?
    let overview: String?
    let posterPath: String?
    let language: String?
    let date: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case overview
        case posterPath = "poster_path"
        case language = "original_language"
        case date = "release_date"
    }
}

//
//  MoviesViewController.swift
//  MovieApp
//
//  Created by Artekium on 14/09/2021.
//

import UIKit

class MoviesViewController: UIViewController {

    @IBOutlet weak var moviesTableView: UITableView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    var viewModel = ViewModelMovies()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureBinding()
    }
    
    private func configureView(){
        moviesTableView.register(MovieTableViewCell.nib(), forCellReuseIdentifier: MovieTableViewCell.identifier)
        moviesTableView.dataSource = self
        moviesTableView.delegate = self
        viewModel.getMovieList()
        loader.startAnimating()
    }
    
    private func configureBinding(){
        viewModel.loadData = { [weak self] () in
            DispatchQueue.main.async {
                self?.moviesTableView.reloadData()
                self?.loader.stopAnimating()
                self?.loader.isHidden = true
            }
        }
    }
}

extension MoviesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dataMovie.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MovieTableViewCell.identifier, for: indexPath) as! MovieTableViewCell
        cell.configureCell(with: viewModel.dataMovie[indexPath.row])
        return cell
    }
}

extension MoviesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let movieId = viewModel.dataMovie[indexPath.row].id {
            let detailView = MovieDetailViewController(nibName: "MovieDetailViewController", bundle: nil)
            detailView.id = movieId
            self.present(detailView, animated: true)
        } else { return }
    }
}

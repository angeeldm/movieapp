//
//  ViewModelMovies.swift
//  MovieApp
//
//  Created by Artekium on 16/09/2021.
//

import UIKit

class ViewModelMovies {
    
    var loadData =  { () -> () in }
    
    var dataMovie: [Movie] = [] {
        didSet { loadData() }
    }
    
     func getMovieList(){
        MoviesService.shared.getMovies(success: {
            (movies) in
            self.dataMovie = movies.results!
        }, failure: {
            (error) in
            let alert = UIAlertController(title: "Algo salio mal", message: "Por favor, intenta de nuevo", preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .default, handler: { action in })
            alert.addAction(action)
            alert.present(alert, animated: true)
        })
    }
}

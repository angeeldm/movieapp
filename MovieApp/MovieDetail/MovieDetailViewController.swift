//
//  MovieDetailViewController.swift
//  MovieApp
//
//  Created by Artekium on 15/09/2021.
//

import UIKit

class MovieDetailViewController: UIViewController {
    var id: Int?

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overviewLabel: UITextView!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.numberOfLines = 0

        MoviesService.shared.getMovieById(id: id!) { [self] (movie) in
            configureView(with: movie)
        } failure: { (error) in
            let alert = UIAlertController(title: "Algo salio mal!", message: "Por favor, intenta de nuevo", preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .default, handler: { action in })
            alert.addAction(action)
            alert.present(alert, animated: true)
        }
    }
    
    static let identifier = "MovieDetailViewController"
    
    static func nib () -> UINib {
        return UINib(nibName: "MovieDetailViewController", bundle: nil)
    }
    
    private func configureView(with movie: Movie){
        titleLabel.text = movie.title
        overviewLabel.text = movie.overview
        dateLabel.text = movie.date
        languageLabel.text = movie.language
    }
}
